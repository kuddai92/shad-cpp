#pragma once
#include <thread>
#include <vector>
#include <iterator>
#include <iostream>
#include <algorithm>


template<class RandomAccessIterator, class T, class Func>
T reduce(RandomAccessIterator first, RandomAccessIterator last, const T& initial_value, Func func) {
    std::vector<std::thread> workers;

    auto nb_threads = static_cast<size_t>(std::thread::hardware_concurrency());
    auto size = static_cast<size_t>(std::distance(first, last));
    if (size == 0 || nb_threads == 0) {
        return initial_value;
    }

    auto nb_chunks = std::min(nb_threads, size);
    auto chunk_size = size / nb_chunks;
    auto rem = size % nb_chunks;
    std::vector<T> results(nb_chunks, initial_value);
    
    for (size_t i = 0; i < nb_chunks; ++i) {
        auto is_last = i == nb_chunks - 1;
        auto start = first + i * chunk_size;
        auto end = first + ((i + 1) * chunk_size - 1 + ((is_last) ? rem : 0));

        workers.emplace_back([&func, &results, i, start, end] {

            auto cur = start;
            auto value = *start;
            while (cur != end) {
                value = func(value, *cur++);
            }
            results[i] = value;
        });
    }

    for (auto& t : workers) {
        t.join();
    }

    auto value(initial_value);
    for (auto& r : results) {
        value = func(value, r);
    }

    return value;
}

