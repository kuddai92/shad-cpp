#pragma once

#include <vector>
#include <initializer_list>

enum class InitType {
    ZERO, ONE
};

template<class T>
class Matrix {
public:
    explicit Matrix(size_t n, InitType init_type = InitType::ZERO): data_(n, std::vector<T>(n)) {
        if (init_type == InitType::ONE) {
            for (size_t i = 0; i < n; ++i)
                data_[i][i] = T(1);
        }
    }

    Matrix(const std::initializer_list<std::initializer_list<T>>& data) {
        size_t n = data.size();
        data_.reserve(n);
        for (const auto& row : data)
            data_.emplace_back(row);
    }

    size_t size() const {
        return data_.size();
    }

    T& operator()(int i, int j) {
        return data_[i][j];
    }

    const T& operator()(int i, int j) const {
        return data_[i][j];
    }
private:
    std::vector<std::vector<T>> data_;
};

template<class T>
const Matrix<T> operator+(const Matrix<T>& l, const Matrix<T>& r) {
    size_t n = l.size();
    Matrix<T> result(n);
    for (size_t i = 0; i < n; ++i)
        for (size_t j = 0; j < n; ++j)
            result(i, j) = l(i, j) + r(i, j);
    return result;
}

template<class T>
const Matrix<T> operator*(const Matrix<T>& a, const T& b) {
    size_t n = a.size();
    Matrix<T> result(n);
    for (size_t i = 0; i < n; ++i)
        for (size_t j = 0; j < n; ++j)
            result(i, j) = a(i, j) * b;
    return result;
}

template<class T>
const Matrix<T> operator*(const T& b, const Matrix<T>& a) {
    return a * b;
}

template<class T>
const Matrix<T> operator-(const Matrix<T>& l, const Matrix<T>& r) {
    return l + r * (-1);
}

template<class T>
const Matrix<T> operator*(const Matrix<T>& l, const Matrix<T>& r) {
    size_t n = l.size();
    Matrix<T> result(n);
    for (size_t i = 0; i < n; ++i)
        for (size_t j = 0; j < n; ++j)
            for (size_t k = 0; k < n; ++k)
                result(i, j) += l(i, k) * r(k, j);
    return result;
}

template<class T>
bool operator==(const Matrix<T>& l, const Matrix<T>& r) {
    size_t n = l.size();
    for (size_t i = 0; i < n; ++i)
        for (size_t j = 0; j < n; ++j)
            if (!(l(i, j) == r(i, j)))
                return false;
    return true;
}

template<class T>
const Matrix<T> transpose(const Matrix<T>& a) {
    Matrix<T> result(a);
    using std::swap;
    size_t n = a.size();
    for (size_t i = 0; i < n; ++i)
        for (size_t j = 0; j < i; ++j)
            swap(result(i, j), result(j, i));
    return result;
}
