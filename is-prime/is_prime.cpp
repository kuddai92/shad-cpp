#include "is_prime.h"
#include <cmath>
#include <thread>
#include <atomic>
#include <algorithm>
#include <vector>

bool is_prime(uint64_t x) {
    if (x <= 1) {
        return false;
    }

    if (x == 2) {
      return true;
    }

    size_t nb_workers = (x > 1000) ? 4 : 1;
    std::atomic<bool> divisible(false);
    std::vector<std::thread> workers;
    uint64_t root = sqrt(x);
    auto max_bound = std::min(root + 6, x);
    auto min_bound = 2ull;
    auto step = (max_bound - min_bound) / nb_workers;
    auto rem = (max_bound - min_bound) % nb_workers;

    for (size_t i = 1; i <= nb_workers; ++i) {
      auto is_last = i == nb_workers;
      auto start_bound = min_bound +  i * step;
      auto end_bound = min_bound + (i + 1) * step + (is_last ? rem : 0); 

      workers.emplace_back([start_bound, end_bound, x, &divisible] {
          for (auto j = start_bound; j <= end_bound; ++j) {
            if (divisible) {
              break;
            }

            if (x % j == 0)  {
              divisible = true;
              break;
            }
          }
      });
    }

    for (auto& t : workers) {
      t.join();
    }

    return !divisible;
}
