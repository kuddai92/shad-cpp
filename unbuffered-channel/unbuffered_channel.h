#pragma once

#include <utility>
#include <optional>
#include <condition_variable>
#include <mutex>
#include <memory>
#include <queue>
#include <stdexcept>


template<class T>
class UnbufferedChannel {
public:
    void send(const T& value) {
        std::unique_lock<std::mutex> lock(mutex_);
        if (is_closed_) {
            throw std::runtime_error("closed");
        }

        std::shared_ptr<Ticket> t;

        if (queue_.empty() || queue_.front()->value) {
            t = std::make_shared<Ticket>(std::make_optional<T>(value));
            queue_.push(t);
        } else {
            t = queue_.front();
            queue_.pop();
            t->value = std::make_optional<T>(value);
            t->cv_read.notify_one();
        }

        t->cv_write.wait(lock, [&t, this]() {
            return is_closed_ || t->consumed;
        });
        
        if (is_closed_ && !t->consumed) {
            throw std::runtime_error("closed");
        }
    }

    std::pair<T, bool> recv() {
        std::unique_lock<std::mutex> lock(mutex_);
        if (is_closed_) {
            return std::make_pair(T(), false);
        }

        std::shared_ptr<Ticket> t;
        
        if (queue_.empty() || !queue_.front()->value) {
            t = std::make_shared<Ticket>(std::nullopt);    
            queue_.push(t);
        } else {
            t = queue_.front(); 
            queue_.pop();
            t->consumed = true;
            t->cv_write.notify_one();
            return std::make_pair(t->value.value(), true);
        }

        t->cv_read.wait(lock, [&t, this]() {
            return is_closed_ || t->value;
        });

        t->cv_write.notify_one();

        if (is_closed_) {
            return std::make_pair(T(), false);
        }

        t->consumed = true;
        return std::make_pair(t->value.value(), true);
    }

    void close() {
        std::unique_lock<std::mutex> lock(mutex_);
        is_closed_ = true;
        while (!queue_.empty()) {
            auto t = queue_.front();
            queue_.pop();
            t->cv_write.notify_one();
            t->cv_read.notify_one();
        }
    }
private:
    struct Ticket {
        Ticket(std::optional<T> v):value(v) {}
        std::optional<T> value;
        std::condition_variable cv_write;
        std::condition_variable cv_read;
        bool consumed = false;
    };

    std::mutex mutex_;
    std::queue<std::shared_ptr<Ticket>> queue_;
    bool is_closed_ = false;
};
