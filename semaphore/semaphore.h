#pragma once

#include <mutex>
#include <queue>
#include <condition_variable>

class DefaultCallback {
public:
    void operator()(int& value) {
        --value;
    }
};

class Semaphore {
public:
    Semaphore(int count): count_(count) {}

    void leave() {
        std::unique_lock<std::mutex> lock(mutex_);
        ++count_;
        cv_.notify_all();
    }

    template<class Func>
    void enter(Func callback) {
        std::unique_lock<std::mutex> lock(mutex_);
        auto idx = idx_++;
        tickets_.push(idx);
        while (count_ == 0 || tickets_.front() != idx)
            cv_.wait(lock);
        tickets_.pop();
        callback(count_);
    }

    void enter() {
        DefaultCallback callback;
        enter(callback);
    }

private:
    std::queue<int> tickets_;
    std::mutex mutex_;
    std::condition_variable cv_;
    int count_ = 0;
    int idx_ = 0;
};

