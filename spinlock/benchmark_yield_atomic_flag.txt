---------------------------------------------------------------
Benchmark                        Time           CPU Iterations
---------------------------------------------------------------
[0;32mrun/real_time/threads:1 [m[0;33m         7 ns          7 ns [m[0;36m  94087896[m
[m[0;32mrun/real_time/threads:2 [m[0;33m         8 ns         15 ns [m[0;36m 102981858[m
[m[0;32mrun/real_time/threads:4 [m[0;33m        12 ns         47 ns [m[0;36m  67694996[m
[m[0;32mrun/real_time/threads:8 [m[0;33m        21 ns        168 ns [m[0;36m  42386592[m
[m