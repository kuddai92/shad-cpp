#pragma once
#include <mutex>
#include <condition_variable>

class RWLock {
public:
    template<class Func>
    void read(Func func) {
        begin_read();
        try {
            func();
        } catch(...) {
            end_read();
            throw;
        }
        end_read();
    }

    template<class Func>
    void write(Func func) {
        std::unique_lock<std::mutex> lock(global_);

        blocked_writers_++;

        while (blocked_readers_ > 0) {
            cv_write_.wait(lock);
        }

        func();
        
        blocked_writers_--;

        if (blocked_writers_ == 0) {
            cv_read_.notify_one();
        } else {
            cv_write_.notify_one();
        }
    }

private:
    std::mutex global_;
    std::condition_variable cv_read_;
    std::condition_variable cv_write_;
    int blocked_readers_ = 0;
    int blocked_writers_ = 0;

    void begin_read() {
        std::unique_lock<std::mutex> lock(global_);
        
        while (blocked_writers_ > 0) {
            cv_read_.wait(lock);
        }

        ++blocked_readers_;
        cv_read_.notify_one();
    }

    void end_read() {
        std::unique_lock<std::mutex> lock(global_);
        --blocked_readers_;
        if (blocked_readers_ == 0 && blocked_writers_ > 0) {
            cv_write_.notify_one(); 
        } 
    }
};
