add_gtest(test_advance test_advance.cpp)
add_gtest(test_set test_set.cpp)

set_property(TARGET test_advance PROPERTY CXX_STANDARD 14)
set_property(TARGET test_set PROPERTY CXX_STANDARD 14)
