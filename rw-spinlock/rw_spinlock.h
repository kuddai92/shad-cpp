#pragma once

#include <atomic>
#include <thread>


class RWSpinLock {
public:
    void lockRead() {
        while (true) {
            unsigned long long mask = ~0ULL ^ 1ULL;
            auto value = counter_.load(std::memory_order_acquire) & mask;
            if (counter_.compare_exchange_weak(value,
                                               value + 2ULL,
                                               std::memory_order_acquire)) {
                break;
            }
            std::this_thread::yield();
        } 
    }

    void unlockRead() {
        counter_.fetch_sub(2ULL, std::memory_order_release);
    }

    void lockWrite() {
        while (true) {
            unsigned long long lock_write_mask = 0ULL;
            auto value = counter_.compare_exchange_weak(lock_write_mask,
                                                        1ULL,
                                                        std::memory_order_acquire);
            if (value) {
                break;
            }
            std::this_thread::yield();
        }
    }

    void unlockWrite() {
        unsigned long long unlock_write_mask = ~0ULL ^ 1ULL;
        counter_.fetch_and(unlock_write_mask, std::memory_order_release);
    }

private:
    std::atomic_ullong counter_ = 0ULL;
};
