#pragma once

#include <atomic>
#include <functional>
#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <stdexcept>
#include <unordered_map>
#include <utility>
#include <vector>

template<class K, class V, class Hash = std::hash<K>>
class ConcurrentHashMap {
public:
    ConcurrentHashMap(const Hash& hasher = Hash()):
        ConcurrentHashMap(kUndefinedSize, hasher) {}

    explicit ConcurrentHashMap(int expected_size, const Hash& hasher = Hash()):
        ConcurrentHashMap(expected_size, kDefaultConcurrencyLevel, hasher) {}

    ConcurrentHashMap(int expected_size, int expected_threads_count, const Hash& hasher = Hash()):
        hasher_(hasher), size_(0) {
        expected_threads_count = std::max(1, expected_threads_count);
        expected_size = (expected_size == kUndefinedSize) ? expected_threads_count : (expected_size / expected_threads_count + 1) * expected_threads_count;

        buckets_.reserve(expected_size);
        for (auto i = 0; i < expected_size; ++i) {
            buckets_.emplace_back();
        }

        nb_locks_ = expected_threads_count;
        locks_.reset(new std::shared_mutex[nb_locks_]);
    }

    bool insert(const K& key, const V& value) {
        auto hash = hasher_(key);
        std::unique_lock<std::shared_mutex> lock (locks_[hash % nb_locks_]);
        auto bucket_id = hash % buckets_.size();

        if (buckets_[bucket_id].find(key) == buckets_[bucket_id].end()) {
            buckets_[bucket_id].emplace(key, value);
            size_.fetch_add(1);
            fix(lock);
            return true;
        } else {
            return false;
        }

    }

    bool erase(const K& key) {
        auto hash = hasher_(key);
        std::unique_lock<std::shared_mutex> lock(locks_[hash % nb_locks_]);
        auto bucket_id = hash % buckets_.size();

        auto it = buckets_[bucket_id].find(key);
        if (it == buckets_[bucket_id].end()) {
            return false;
        } else {
            buckets_[bucket_id].erase(it);
            size_.fetch_add(-1);
            return true;
        }
    }

    void clear() {
        lockBuckets();
        std::vector<Bucket> expected;
        expected.reserve(buckets_.size());
        for (size_t i = 0; i < buckets_.size(); ++i) {
            expected.emplace_back();
        }
        std::swap(buckets_, expected);
        size_ = 0ul;
        unlockBuckets();
    }

    std::pair<bool, V> find(const K& key) const {
        auto hash = hasher_(key);
        std::shared_lock<std::shared_mutex> lock(locks_[hash % nb_locks_]);
        auto bucket_id = hash % buckets_.size();

        auto it = buckets_[bucket_id].find(key);
        if (it == buckets_[bucket_id].end()) {
            return {false, V()};
        } else {
            return {true, it->second};
        }
    }

    const V at(const K& key) const {
        auto p = find(key);
        if (!p.first) {
            throw std::out_of_range("key is not found");
        }
        return p.second;
    }

    size_t size() const {
        return size_;
    }

    static const int kDefaultConcurrencyLevel;
    static const int kUndefinedSize;
    static const double kMaxLoad;
private:
    typedef std::map<K, V> Bucket;
    std::vector<Bucket> buckets_;
    mutable std::unique_ptr<std::shared_mutex[]> locks_;
    size_t nb_locks_;
    Hash hasher_;
    std::atomic<size_t> size_;

    void fix(std::unique_lock<std::shared_mutex>& lock) {
        auto load = calcLoad();
        lock.unlock();
        if (load < kMaxLoad) {
            return;
        }
        lockBuckets();
        if (calcLoad() < kMaxLoad) {
            unlockBuckets();
            return;
        }
        rehash();
        unlockBuckets();
    }

    void rehash() {
        // std::cout << "rehash, load " << calcLoad() << std::endl;
        // std::cout << "size_ " << size_ << std::endl;
        // std::cout << "nb buckets " << buckets_.size() << std::endl;
        std::vector<Bucket> extended;
        extended.reserve(buckets_.size() * 2UL);
        for (size_t i = 0; i < buckets_.size() * 2UL; ++i) {
            extended.emplace_back();
        }

        for (auto& bucket : buckets_) {
            for (auto it = bucket.begin(); it != bucket.end(); ++it) {
                size_t hash = hasher_(it->first);
                extended[hash % extended.size()].emplace(std::move(*it));
            }
        }    
        std::swap(buckets_, extended);
    }

    double calcLoad() {
        return static_cast<double>(size_) / buckets_.size();
    };

    void lockBuckets() {
        for (size_t i = 0; i < nb_locks_; ++i) {
            locks_[i].lock();
        }
    }

    void unlockBuckets() {
        for (size_t i = 0; i < nb_locks_; ++i) {
            locks_[i].unlock();
        }
    }
};

template<class K, class V, class Hash>
const int ConcurrentHashMap<K, V, Hash>::kDefaultConcurrencyLevel = 8;

template<class K, class V, class Hash>
const int ConcurrentHashMap<K, V, Hash>::kUndefinedSize = -1;

template<class K, class V, class Hash>
const double ConcurrentHashMap<K, V, Hash>::kMaxLoad = 0.5;
