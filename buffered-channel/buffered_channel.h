#pragma once

#include <utility>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <stdexcept>
#include <iostream>

template<class T>
class BufferedChannel {
public:
    explicit BufferedChannel(int size):
        size_(size), nb_senders_(0), nb_readers_(0), is_closed_(false) {}
    void send(const T& value) {
        std::unique_lock<std::mutex> lock(mutex_);
        if (is_closed_) {
            throw std::runtime_error("buffer is closed");
        }

        nb_senders_++;
        cv_write_.wait(lock, [this]() {
            return queue_.size() < size_;
        });
        nb_senders_--;
        queue_.push(value);
        cv_read_.notify_one();
    }

    std::pair<T, bool> recv() {
        std::unique_lock<std::mutex> lock(mutex_);

        nb_readers_++;
        cv_read_.wait(lock, [this]() {
            long pending = static_cast<long>(queue_.size()) + nb_senders_;
            if (is_closed_ && pending == 0) {
                return true;
            } 

            return !queue_.empty();
        });
        nb_readers_--;

        if (is_closed_) {
            cv_read_.notify_one();
        }

        if (queue_.empty()) {
            return std::make_pair(T(), false);
        }

        auto pair = std::make_pair(queue_.front(), true);
        cv_write_.notify_one();
        queue_.pop();
        return pair;
    }

    void close() {
        std::unique_lock<std::mutex> lock(mutex_);
        is_closed_ = true;
        cv_read_.notify_all();
    }

private:
    std::size_t size_;
    long nb_senders_;
    long nb_readers_;
    std::queue<T> queue_;
    std::mutex mutex_;
    std::condition_variable cv_read_;
    std::condition_variable cv_write_;
    bool is_closed_;
};
