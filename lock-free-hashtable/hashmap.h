#pragma once

#include <cassert>
#include <atomic>
#include <vector>
#include <functional>
#include <exception>
#include <optional>
#include <iostream>


template<class K, class V, class Hash = std::hash<K>>
class ConcurrentHashMap {
public:
    ConcurrentHashMap(size_t max_size, K default_key):
        buckets_(max_size),
        default_key_(default_key) {
        for (auto& v : buckets_) {
            v.first.store(default_key_, std::memory_order_relaxed);
        }
    }

    ConcurrentHashMap(size_t max_size, K default_key, K erased_key): ConcurrentHashMap(max_size, default_key) {
        erased_key_ = std::make_optional(erased_key);
    }

    void insert(K key, const V& value) {
        validate(key);

        size_t hash = hash_func_(key);

        for (size_t shift = 0; shift < buckets_.size(); ++shift) {
            size_t idx = (hash  + shift) % buckets_.size();

            K probed = buckets_[idx].first.load(std::memory_order_relaxed);
            if (probed == default_key_ || (erased_key_ && probed == erased_key_.value()) || probed == key) {
                const bool success = probed == key || buckets_[idx].first.compare_exchange_strong(
                    probed,
                    key,
                    std::memory_order_relaxed
                );

                if (success) {
                    buckets_[idx].second.store(value, std::memory_order_relaxed);
                    return;
                }
            }
        }
    }

    std::pair<bool, V> find(K key) const {
        validate(key);

        size_t hash = hash_func_(key);
        for (size_t shift = 0; shift < buckets_.size(); ++shift) {
            size_t idx = (hash  + shift) % buckets_.size();

            K probed = buckets_[idx].first.load(std::memory_order_relaxed);
            if (probed == key) {
                return std::make_pair(true, buckets_[idx].second.load());
            }

            if (probed == default_key_) {
                return std::make_pair(false, V());
            }
        }

        return std::make_pair(false, V());
    }

    bool erase(K key) {
        validate(key);
        validate_erase_op();

        size_t hash = hash_func_(key);

        for (size_t shift = 0; shift < buckets_.size(); ++shift) {
            size_t idx = (hash  + shift) % buckets_.size();

            K probed = buckets_[idx].first.load(std::memory_order_relaxed);
            if (probed == key) {
                return buckets_[idx].first.compare_exchange_strong(
                    probed,
                    erased_key_.value(),
                    std::memory_order_relaxed
                );
            }

            if (probed == default_key_) {
                return false;
            }
        }

        return false;
    }

private:
    std::vector<std::pair<std::atomic<K>, std::atomic<V>>> buckets_;
    K default_key_;
    std::optional<K> erased_key_;
    Hash hash_func_;

    void validate(const K& key) const {
        if (key == default_key_ || (erased_key_ && key == erased_key_.value())) {
            throw std::runtime_error("invalid key");
        }
    }

    void validate_erase_op() const {
        if (!erased_key_) {
            throw std::runtime_error("no erase key given");
        }
    }
};
