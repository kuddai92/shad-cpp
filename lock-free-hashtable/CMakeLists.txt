cmake_minimum_required(VERSION 2.8)
project(lock-free-hashtable)

if (TEST_SOLUTION)
  include_directories(../private/lock-free-hashtable/fast)
endif()

include(../common.cmake)

add_gtest(test_hashtable test.cpp)

add_benchmark(bench_hashtable run.cpp)
